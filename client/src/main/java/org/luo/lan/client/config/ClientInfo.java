package org.luo.lan.client.config;

import lombok.Data;
/**
 * @Auther: luobiao
 * @Date: 2020/9/19 09:15
 * @Description:
 */
@Data
public class ClientInfo {
    private String key;
    private String server_address;
    private Integer server_port;
    private Integer proxy_port;
    private String ssl_pwd;
    private String ssl_file_name;
}
