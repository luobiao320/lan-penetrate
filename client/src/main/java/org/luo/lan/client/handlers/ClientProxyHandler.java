package org.luo.lan.client.handlers;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;
import org.luo.lan.client.constants.AttrConstants;
import org.luo.lan.client.factory.SessionFactory;
import org.luo.lan.common.handler.ControlTypeConstant;
import org.luo.lan.common.handler.Request;
import org.luo.lan.common.util.StringUtils;

/**
 * @Auther: luobiao
 * @Date: 2020/9/19 09:15
 * @Description:
 */
@Slf4j
public class ClientProxyHandler extends SimpleChannelInboundHandler<ByteBuf> {
    private NioSocketChannel bridgeChannel;

    public ClientProxyHandler(NioSocketChannel bridgeChannel) {
        this.bridgeChannel = bridgeChannel;
    }

    @Override
    public void channelRead0(ChannelHandlerContext ctx, ByteBuf byteBuf) {
        byte[] bytes=new byte[byteBuf.readableBytes()];
        NioSocketChannel channel=(NioSocketChannel) ctx.channel();
        byteBuf.readBytes(bytes);
        log.debug("代理请求的channel={}，：",channel);
/*        log.debug("---------------------------------代理请求内容begin---------------------------------------");
        log.debug(new String(bytes));
        log.debug("-----------------------------------代理请求内容end-------------------------------------");*/

        String sessionKey = channel.attr(AttrConstants.SEESION_KEY).get();
        bridgeChannel.writeAndFlush(Request.SUCCESS(ControlTypeConstant.PROXY, bytes).addHeader(sessionKey));
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        NioSocketChannel channel=(NioSocketChannel) ctx.channel();
        String sessionKey=SessionFactory.INSTANCE.createSession(channel);
        channel.attr(AttrConstants.SEESION_KEY).set(sessionKey);
        channel.attr(AttrConstants.IS_MANUAL_CLOSE).set(false);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx){
        NioSocketChannel channel=(NioSocketChannel)ctx.channel();
        String sessionKey=channel.attr(AttrConstants.SEESION_KEY).get();
        if (StringUtils.isNotBlank(sessionKey)) {
            Boolean isManualClose=channel.attr(AttrConstants.IS_MANUAL_CLOSE).get();
            if (!isManualClose) {
                Channel bridgeChannel=channel.attr(AttrConstants.BRIDGE_CHANNEL).get();
                if (bridgeChannel != null) {
                    bridgeChannel.writeAndFlush(Request.SUCCESS(ControlTypeConstant.CLOSE_SESSION, "客户端请求关闭代理会话通道！").addHeader(sessionKey));
                }
            }
            SessionFactory.INSTANCE.removeSession(sessionKey);
        }
    }
}
