package org.luo.lan.client.config;

import org.luo.lan.common.config.AbstractConfig;
import org.luo.lan.common.Constant;
import org.luo.lan.common.util.Pair;
import org.luo.lan.common.util.StringUtils;

/**
 * @Auther: luobiao
 * @Date: 2020/9/19 09:15
 * @Description:
 */
public class ClientConfig extends AbstractConfig<ClientInfo> {
    private static ClientConfig instance = null;

    private ClientConfig(){
        this.configFileName = Constant.CLIENT_CONFIG_FILE_NAME;
        this.configInfoClass=ClientInfo.class;
        init();
    }

    public static ClientConfig getInstance(){
        if (instance == null) {
            synchronized(ClientConfig.class) {
                if (instance == null)
                    instance = new ClientConfig();
            }
        }
        return instance;
    }

    public synchronized int getServerPort(){
        return this.config.getServer_port();
    }

    public synchronized int getProxyPort(){
        if (config.getProxy_port() == null) {
            return 0;
        }else{
            return config.getProxy_port();
        }
    }

    public synchronized String getServerAddress(){
        return this.config.getServer_address();
    }

    public synchronized String getKey(){
        return this.config.getKey();
    }

    public synchronized String getSslFileName(){
        return Constant.USER_DIR + this.config.getSsl_file_name();
    }

    public synchronized String getSslPwd(){
        return this.config.getSsl_pwd();
    }

    @Override
    protected Pair<Boolean, String> checkConfig(ClientInfo clientInfo) {
        Pair pair = new Pair(true, "配置校验通过！");
        if (StringUtils.isBlank(clientInfo.getKey())) {
            return new Pair<>(false, "key不能为空");
        }
        return pair;
    }
}
