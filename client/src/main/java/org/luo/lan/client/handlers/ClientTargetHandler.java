package org.luo.lan.client.handlers;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;
import org.luo.lan.client.constants.AttrConstants;
import org.luo.lan.client.factory.SessionFactory;
import org.luo.lan.common.handler.ControlTypeConstant;
import org.luo.lan.common.handler.Request;
import org.luo.lan.common.util.StringUtils;

/**
 * @Auther: luobiao
 * @Date: 2020/9/19 09:15
 * @Description:
 */
@Slf4j
public class ClientTargetHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg){
        ByteBuf byteBuf=(ByteBuf) msg;
        byte[] bytes=new byte[byteBuf.readableBytes()];
        byteBuf.readBytes(bytes);
        NioSocketChannel channel=(NioSocketChannel) ctx.channel();
        String sessionKey=channel.attr(AttrConstants.SEESION_KEY).get();
        NioSocketChannel bridgeChannel=channel.attr(AttrConstants.BRIDGE_CHANNEL).get();
        bridgeChannel.writeAndFlush(Request.SUCCESS(ControlTypeConstant.TRANSPORT, bytes).addHeader(sessionKey));
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx){
        NioSocketChannel channel=(NioSocketChannel)ctx.channel();
        String sessionKey=channel.attr(AttrConstants.SEESION_KEY).get();
        if (StringUtils.isNotBlank(sessionKey)) {
            Boolean isManualClose=channel.attr(AttrConstants.IS_MANUAL_CLOSE).get();
            if (!isManualClose) {
                Channel bridgeChannel=channel.attr(AttrConstants.BRIDGE_CHANNEL).get();
                if (bridgeChannel != null) {
                    bridgeChannel.writeAndFlush(Request.SUCCESS(ControlTypeConstant.CLOSE_SESSION, "客户端请求关闭目标会话通道！").addHeader(sessionKey));
                }
            }
            SessionFactory.INSTANCE.removeSession(sessionKey);
        }
    }
}
