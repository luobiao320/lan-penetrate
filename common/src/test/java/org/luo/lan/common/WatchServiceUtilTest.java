package org.luo.lan.common;

import org.junit.Test;
import org.luo.lan.common.util.WatchServiceUtil;


public class WatchServiceUtilTest {
    @Test
    public void fileChangeTest() {
        String dir = "D://temp";
        new Thread(()->{
            WatchServiceUtil.watchDirChange(dir,(t)->{
                t.stream().forEach(f->{
                    System.out.println(f);
                });
            });
        }).start();

    }
}
