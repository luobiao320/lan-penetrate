package org.luo.lan.common;

import org.junit.Test;
import org.luo.lan.common.config.SslConfig;
import org.luo.lan.common.util.SslEngineUtil;

import javax.net.ssl.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.ByteBuffer;
import java.security.KeyStore;
import java.security.KeyStoreException;

/**
 * @Auther: luobiao
 * @Date: 2020/9/19 16:59
 * @Description:
 */
public class SslEngineUtilTest {
    @Test
    public void test() throws SSLException {

        SslConfig sslConfig = SslConfig.builder("123456", Constant.USER_DIR + "/" + "client.jks").build();
        SSLEngine sslEngine = SslEngineUtil.getSslEngine(sslConfig);
        sslEngine.setUseClientMode(true);
        ByteBuffer in = ByteBuffer.allocateDirect(sslEngine.getSession().getPacketBufferSize());

        ByteBuffer clientOut = ByteBuffer.wrap("Hi Server, I'm Client".getBytes());
        ByteBuffer[] byteBuffers = new ByteBuffer[1];
        byteBuffers[0] = clientOut;
        SSLEngineResult result = sslEngine.wrap(byteBuffers, in);
        System.out.println(result.bytesProduced());

    }
}
