package org.luo.lan.common.handler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractHanderController<T> implements HanderType<T>, Controller<T> {
    public void control(T ctx, Request request) {
        byte type = request.getType();
        switch (type) {
            case ControlTypeConstant.AUTH:
                auth(ctx, request);
                break;
            case ControlTypeConstant.TRANSPORT:
                transport(ctx, request);
                break;
            case ControlTypeConstant.HEARTBEAT:
                heartbeat(ctx, request);
                break;
            case ControlTypeConstant.CLOSE_SESSION:
                closeSession(ctx, request);
                break;
            case ControlTypeConstant.PROXY:
                proxy(ctx, request);
                break;
            default:
                log.info("contrl defalut ....");
        }
    }

}
