package org.luo.lan.common;

import lombok.extern.slf4j.Slf4j;

/**
 * @Auther: luobiao
 * @Date: 2020/10/17 10:36
 * @Description:
 */
@Slf4j
public class WaitWriteTask implements Runnable{
    private Runnable task;
    private byte run;

    public WaitWriteTask(Runnable task) {
        this.task = task;
    }

    @Override
    public void run() {
        task.run();
        run = 1;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (run == 0) {
            //主要是打印出那些在sessionKey对应的queue被移除后添加到queue中的任务
            log.info(this+"写入任务未被执行！");
        }
    }
}
