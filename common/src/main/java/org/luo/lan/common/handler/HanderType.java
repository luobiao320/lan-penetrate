package org.luo.lan.common.handler;

public interface HanderType<T> {
    void auth(T ctx, Request request);
    void transport(T ctx, Request request);
    void heartbeat(T ctx, Request request);
    void closeSession(T ctx, Request request);
    void proxy(T ctx, Request request);
}
