package org.luo.lan.common;
/**
 * @Auther: luobiao
 * @Date: 2020/9/19 09:15
 * @Description:
 */
public interface Constant {
    String USER_DIR = System.getProperty("user.dir") + "/";
    String SERVER_CONFIG_FILE_NAME = "server_config.json";//服务器端配置文件的名称
    String CLIENT_CONFIG_FILE_NAME = "client_config.json";//客户端端配置文件的名称
    String SERVER_SSL_FILE_NAME = "server.jks";//服务器端ssl配置文件
    String CLIENT_SSL_FILE_NAME = "client.jks";//客户端ssl配置文件

}
