package org.luo.lan.common.util;

import java.io.File;
import java.util.function.Supplier;
/**
 * @Auther: luobiao
 * @Date: 2020/9/19 09:15
 * @Description:
 */
public abstract class Assert {
    public static void notEmpty(String text, String message) {
        if (StringUtils.isEmpty(text)) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void notEmpty( String text, Supplier<String> messageSupplier) {
        if (StringUtils.isEmpty(text)) {
            throw new IllegalArgumentException(nullSafeGet(messageSupplier));
        }
    }

    public static void notBlank( String text, String message) {
        if (StringUtils.isBlank(text)) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void notBlank( String text, Supplier<String> messageSupplier) {
        if (StringUtils.isBlank(text)) {
            throw new IllegalArgumentException(nullSafeGet(messageSupplier));
        }
    }

    public static void fielExist( File file,String message) {
        if (file !=null&&!file.exists()) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void fielExist(File file, String message, String ... params) {
        fielExist(file, String.format(message, params));
    }



    private static String nullSafeGet( Supplier<String> messageSupplier) {
        return (messageSupplier != null ? messageSupplier.get() : null);
    }
}
