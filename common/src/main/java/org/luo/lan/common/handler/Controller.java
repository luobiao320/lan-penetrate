package org.luo.lan.common.handler;

public interface Controller<T> {
    void control(T t, Request request);
}
