
package org.luo.lan.common.config;

public class SslConfig {
    private String pwd;
    private String jksFilePath;
    private Boolean useClientMode;
    private Boolean needClientAuth;

    SslConfig(String pwd, String jksFilePath, boolean useClientMode, boolean needClientAuth) {
        this.pwd = pwd;
        this.jksFilePath = jksFilePath;
        this.useClientMode = useClientMode;
        this.needClientAuth = needClientAuth;
    }

    public static SslConfig.SslConfigBuilder builder(String pwd, String jksFilePath) {
        return new SslConfig.SslConfigBuilder(pwd,jksFilePath);
    }

    public String getPwd() {
        return this.pwd;
    }

    public String getJksFilePath() {
        return this.jksFilePath;
    }

    public Boolean isUseClientMode() {
        return this.useClientMode;
    }

    public Boolean isNeedClientAuth() {
        return this.needClientAuth;
    }

    public static class SslConfigBuilder {
        private String pwd;
        private String jksFilePath;
        private boolean useClientMode;
        private boolean needClientAuth;

        SslConfigBuilder(String pwd, String jksFilePath) {
            this.pwd=pwd;
            this.jksFilePath = jksFilePath;
        }

        public SslConfig.SslConfigBuilder useClientMode(boolean useClientMode) {
            this.useClientMode = useClientMode;
            return this;
        }

        public SslConfig.SslConfigBuilder needClientAuth(boolean needClientAuth) {
            this.needClientAuth = needClientAuth;
            return this;
        }

        public SslConfig build() {
            return new SslConfig(this.pwd,this.jksFilePath,this.useClientMode, this.needClientAuth);
        }

        public String toString() {
            return "SslConfig.SslConfigBuilder(pwd=" + this.pwd + ", jksFilePath=" + this.jksFilePath + ", useClientMode=" + this.useClientMode + ", needClientAuth=" + this.needClientAuth + ")";
        }
    }
}
