package org.luo.lan.common.handler;
/**
 * @Auther: luobiao
 * @Date: 2020/9/19 09:15
 * @Description:
 */
public class ControlTypeConstant {
    public final static byte AUTH = 1;
    public final static byte TRANSPORT = 2;
    public final static byte HEARTBEAT = 3;
    public final static byte CLOSE_SESSION = 4;
    public final static byte PROXY = 5;
}
