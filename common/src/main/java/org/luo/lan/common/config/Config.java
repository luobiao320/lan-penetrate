package org.luo.lan.common.config;
/**
 * @Auther: luobiao
 * @Date: 2020/9/19 09:15
 * @Description:
 */
public interface Config<T> {
    T init();
    void onChange() throws InterruptedException;
}
