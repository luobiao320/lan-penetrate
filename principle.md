#### lan-penetrate 实现原理（此处只讲域名代理，tcp隧道原理差不多）

此处依旧以默认配置进行说明，这里再次贴出服务器端的配置

![输入图片说明](https://images.gitee.com/uploads/images/2020/1028/222209_00cbb063_421331.png "屏幕截图.png")

针对默认配置，简单的画了个请求原理图 [原图连接](https://www.processon.com/view/link/5f6f507f5653bb6013254af8)

![输入图片说明](https://images.gitee.com/uploads/images/2020/1028/161111_fb4812a3_421331.png "屏幕截图.png")

下面逐步进行说明

1. 读取服务器端配置

    在启动类ServerApplication被执行的时候会触发端口监听，不过在此之前会先去加载服务器配置，服务器配置是通过单例类ServerConfig进行管理。
    
    在配置文件读取完成后会有个线程监听配置文件的修改（具体代码查看WatchServiceUtil#watchDirChange），当某个客户端部分的配置被修改时会断开对应的客户端连接，然后由客户端进行重连

2. 服务器端启动

    上一步读取完配置之后，会取出bridge_port（8024）和domain_proxy_port（9527）这两个配置的端口进行监听。8024负责监听客户端的连接（对应图中的ServerBridge），9527负责监听浏览器的代理请求对应图中的（ServerProxy）
    
3. 客户端启动

    在客户端启动时会从客户端配置中读取连接的服务器地址（server_address），并和服务器建立tcp连接。当tcp连接建立后服务器端会创建一个对应的通道（对图中服务器部分的BridgeChannel）
    
    在客户端连接建立后客户端会每隔一段时间发送一次心跳包，如果如果服务器端超过一定次数都没有进行响应则会重新建立tcp连接（具体代码查看ClientBridgeHandler#userEventTriggered）
    
4. 浏览器请求

   当浏览器发起 http://lan1.luobiao.vip:9527 请求时，会被9527端口监听到，ServerProxy会创建一个通道进行处理该http请求（对应图片中服务器部分的proxyChannel）
   
   在通道连接建立后，真正进行http数据传送的时候会进入AbstractServerProxyHandler#channelRead0方法，该方法中会解析出请求的域名，然后通过域名从配置文件中找到对应的客户端及目标主机+端口，最终数据会通过BridgeChannel传送到客户端，然后再由客户端将数据传送到对应的目标主机端口
   
5. 数据返回   

    在4中创建proxyChannel的时候会生成一个唯一的sessionKey，每个请求都会携带这个key，在目标主机返回数据到服务器端的时候就会通过该key找到对应的proxyChannnel，这部分的代码可查看ServerBridgeHandler#transport
 
   