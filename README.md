# lan-penetrate

#### 介绍
该项目主要实现了内网穿透功能，主要可以应用于：

1. 实现微信等第三方公网回调请求进入到本机开发调试
  
2. 访问内网资源（如在家访问公司内网的gitlab完成代码提交和拉取操作）

3. 通过tcp隧道功能实现诸如远程桌面控制等功能（比如使用家里电脑通过window内置的mstsc远程控制公司电脑window电脑）

4. 使用代理功能加速国外站点访问，该功能你懂得 ^o^

#### 软件架构
项目主要分为4部分

client: 客户端

common: 客户端和服务器端公用的一些东西

parent: 公共的maven依赖包版本管理

server: 服务器端

具体的实现原理请查看 [原理篇](https://gitee.com/luobiao320/lan-penetrate/blob/master/principle.md)

由于是基于netty进行实现的，而且会涉及到一些网络方面的知识，建议了解下这方面的知识。本人在这个项目之前也是先做了些知识储备和笔记，有需要的童学可以参考看下“网络相关”部分的内容。 [processon 脑图链接汇总](https://www.processon.com/view/link/5f9923370791291771351abf)

> 注意：笔记中记载的一些源码分析相关的内容是本人在调试或查找其他资料后的一些理解，难免会有理解错误的地方，仅供参考，也欢迎进行指正

![输入图片说明](https://images.gitee.com/uploads/images/2020/1028/154321_9b3a11af_421331.png "屏幕截图.png")

[processon 脑图链接汇总](https://www.processon.com/view/link/5f9923370791291771351abf)

#### 安装教程

##### 服务器端安装（linux）

> 没有公网服务器的童学如果想进行联调测试的可下方评论中联系我（服务器端安装这一步可跳过）。
我可以帮忙分配一个二级域名xxx.luobiao.vip，不过出于安全性和稳定性（我的个人服务器配置比较低且在香港延时会比较高）还是建议使用自己的服务器

  1. 下载服务器端压缩包上传到有公网ip的服务器 [下载地址](https://gitee.com/luobiao320/lan-penetrate/attach_files/531703/download/server-1.1.0.zip)
    
  2. 在服务器端将压缩包进行解压，解压后将切换到server目录中，该目录中文件结构如下

     ![](https://images.gitee.com/uploads/images/2020/1028/114011_02f0f636_421331.png "屏幕截图.png")

  3. 修改server_config.json中的默认配置为自定义配置，默认配置如下

     ![输入图片说明](https://images.gitee.com/uploads/images/2020/1028/222209_00cbb063_421331.png "屏幕截图.png")

     bridge_port：客户端和服务器端的桥接端口，该端口需要开放能被公网

     domain_proxy_port：域名代理端口，如果该端口不想暴露到公网，可通过nginx代理的该端口上（注意如果通过https访问则必须要代理到该端口前处理好证书问题）

     ssl_pwd和ssl_file_name：这两个配置主要是用来对客户端和服务器端数据传输进行ssl加密的，如有需要可以自己生成证书进行替换该配置
java ssl证书生成可参考 [链接#1.1.2.2](https://www.processon.com/view/link/5f4cbfbd7d9c082a6bab9812#map)

     6a143929-5ef5-487b-926d-e2d1ce49b611：客户端的key，该key可以通过执行sh key_genarate.sh获取

     domainList：域名解析列表，其中host是请求的域名，target是域名所要访问的目标主机+端口，该主机可以是启动客户端的主机或和启动客户端主机在同一网段的主机

     tcpTunnelList：tcp隧道列表，其中port对应的是隧道的监听端口，target是该隧道最终要访问的目标主机+加端口，和域名解析一样，该主机可以是启动客户端的主机或和启动客户端主机在同一网段的主机



  4. 在当前server目录中使用sh start.sh命令启动服务器端，在服务启动后对配置中客户端部分的修改将自动进行加载

##### 客户端（windows）

  1. 下载客户端压缩包 [下载地址](https://gitee.com/luobiao320/lan-penetrate/attach_files/531702/download/client-1.1.0.zip)

  2. 将下载的客户端压缩包放在最终需要远程访问的目标主机上进行解压缩，解压后的文件目录如下

     ![输入图片说明](https://images.gitee.com/uploads/images/2020/1028/142114_dc109760_421331.png "屏幕截图.png")

  3. 修改client_config中的配置，默认配置如下

     ![输入图片说明](https://images.gitee.com/uploads/images/2020/1028/143801_b4672353_421331.png "屏幕截图.png")

     key：客户端key，由服务器端的key_genarate.sh脚本生成，也可以是自定义的一个key（不建议这么做），不过要和服务器端对应上（此处对应默认配置中的“我的客户端1”的key）

     server_address：服务器端公网地址

     server_port：桥接端口，要和服务器端的bridge_port端口对应

4. 双击start.bat启动客户端

#### 使用示例（以上面安装教程中的默认配置进行说明）

1. 实现微信公众号的回调接口进入本机开放调试 
    
   假设本机项目启动的是8080端口，准备使用/api/wechat/callback接口作为微信事件回调接口
    
   那么微信微信公众号中的接口配置为（我此处为测试公众号）：
    
    ![输入图片说明](https://images.gitee.com/uploads/images/2020/1028/150047_9ec30e9d_421331.png "屏幕截图.png")

   这样对这个公众号的所有事件回调都将进入本机的http://localhost:8080//api/wechat/callback接口
   
2. 实现家里电脑对公司电脑的远程桌面控制（此处以windows远程控制为例）

    > 在默认配置“我的客户端1”中我们开启了一个9528的tcp隧道
    
    首先我们将“我的客户端1”客户端在公司电脑上启动，接着我们在家里的电脑中打开系统自带的mtsc，然后填入如下连接信息就可以远程操控公司电脑了
    
    ![输入图片说明](https://images.gitee.com/uploads/images/2020/1028/151839_0349979c_421331.png "屏幕截图.png")
    
    ![输入图片说明](https://images.gitee.com/uploads/images/2020/1028/151939_9cb22154_421331.png "屏幕截图.png")

    注意：
        
      1. 被操控的电脑一定要设置一个复杂点的密码哦，不然。。。
      2. 在使用远程桌面的时候不要在桌面上通过shell查看服务器请求日志，不然会陷入死循环

3. 使用代理功能加速访问国外站点

    > 服务器端要部署在境外哦
    
    在客户端中配置中新增proxy_port端口配置，后面所以的请求都会被该端口代理到服务器，然后由服务器端去访问相应的站点
    
    ![输入图片说明](https://images.gitee.com/uploads/images/2020/1108/225835_09bc8a69_421331.png "屏幕截图.png")

    启动客户端后会多出一个proxy_prot配置的监听端口，剩下的只需要在浏览器中配置好代理就行了
    
    ![输入图片说明](https://images.gitee.com/uploads/images/2020/1108/225900_13a04d80_421331.png "屏幕截图.png")

    
    
   