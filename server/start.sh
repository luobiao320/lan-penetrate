#!/bin/bash

log="./logs"

if [ ! -d $log ];then
   mkdir -p $log
fi

nohup java -jar ./lib/lan-penetrate-server.jar > ./logs/catalina.log &
sleep 1
tail -fn 50 ./logs/catalina.log
