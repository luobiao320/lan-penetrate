package org.luo.lan.server.util;

import lombok.Data;

/**
 * @Auther: luobiao
 * @Date: 2020/11/1 19:23
 * @Description:
 */
@Data
public class HostReplaceInfo {
    private Integer hostStartIndex;
    private String oldTarget;
    private String newTarget;

    public HostReplaceInfo(Integer hostStartIndex, String oldTarget, String newTarget) {
        this.hostStartIndex = hostStartIndex;
        this.oldTarget = oldTarget;
        this.newTarget = newTarget;
    }
}
