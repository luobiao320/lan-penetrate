package org.luo.lan.server.config;

import lombok.Data;
import lombok.Getter;

import java.util.List;
import java.util.Map;

/**
 * @Auther: luobiao
 * @Date: 2020/9/19 09:15
 * @Description:
 */
@Data
public class ServerInfo {
    private Server server;
    private Map<String,Client> clientMap;

    @Data
    public static class Server{
        private Integer domain_proxy_port;
        private String proxy_ip;
        private Integer bridge_port;
        private String bridge_ip;
        private String ssl_pwd;
        private String ssl_file_name;
    }

    @Data
    public static class Client{
        private String name;
        private List<Domain> domainList;
        private List<TcpTunnel> tcpTunnelList;
    }

    @Data
    public static class Domain{
        private String host;
        private String target;
    }

    @Data
    public static class TcpTunnel{
        private Integer port;
        private String target;
    }
}
