package org.luo.lan.server.enums;

import org.luo.lan.common.util.Pair;
import org.luo.lan.server.config.ServerConfig;
import org.luo.lan.server.config.ServerInfo;

import java.util.ArrayList;
import java.util.List;

public enum  ProxyTypeEnum {
    DO_MAIN("域名解析"),
    TCP_TUNNEL("tcp隧道");

    private String desc;
    ProxyTypeEnum(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public List<Pair<String,String>> getProxyKeys(ServerInfo.Client client) {
        List<Pair<String,String>> list = new ArrayList<>();
        switch (name()) {
            case "DO_MAIN":
                List<ServerInfo.Domain> domainList=client.getDomainList();
                if (domainList != null) {
                    domainList.stream().forEach(p-> list.add(new Pair<>(p.getHost(),p.getTarget())));
                }
                break;
            case "TCP_TUNNEL":
                List<ServerInfo.TcpTunnel> tcpTunnelList=client.getTcpTunnelList();
                if (tcpTunnelList != null) {
                    tcpTunnelList.stream().forEach(p-> list.add(new Pair(String.valueOf(p.getPort()),p.getTarget())));
                }
                break;
            default:
        }
        return list;
    }

}
