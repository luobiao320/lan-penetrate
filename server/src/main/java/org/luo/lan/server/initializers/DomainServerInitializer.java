package org.luo.lan.server.initializers;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import org.luo.lan.server.handlers.DomainServerHandler;

public class DomainServerInitializer extends ChannelInitializer<SocketChannel> {
    @Override
    public void initChannel(SocketChannel ch) {
        ch.pipeline().addLast(new DomainServerHandler());
    }
}
