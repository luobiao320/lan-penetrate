package org.luo.lan.server.handlers;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TcpTunnelServerHandler extends AbstractServerHandler {
    @Override
    String getProxyKey(ChannelHandlerContext ctx,ByteBuf byteBuf) {
        NioSocketChannel channel = (NioSocketChannel) ctx.channel();
        String port=String.valueOf(channel.localAddress().getPort());
        return port;
    }

    @Override
    byte[] getBytes(ByteBuf byteBuf) {
        byte[] bytes = new byte[byteBuf.readableBytes()];
        byteBuf.readBytes(bytes);
        return bytes;
    }
}
