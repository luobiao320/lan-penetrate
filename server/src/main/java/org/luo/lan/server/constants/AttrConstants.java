package org.luo.lan.server.constants;

import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.util.AttributeKey;

public interface AttrConstants {
    AttributeKey<NioSocketChannel> BRIDGE_CHANNEL = AttributeKey.newInstance("BRIDGE_CHANNEL");
    AttributeKey<String> SEESION_KEY = AttributeKey.newInstance("SEESION_KEY");
    AttributeKey<String> CLIENT_KEY = AttributeKey.newInstance("CLIENT_KEY");
    AttributeKey<String> TARGET = AttributeKey.newInstance("TARGET");
    //是否是手动触发的关闭操作
    AttributeKey<Boolean> IS_MANUAL_CLOSE = AttributeKey.newInstance("IS_CLIENT_CLOSE");
}
